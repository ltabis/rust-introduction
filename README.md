# Rust introduction

- my github: <https://github.com/ltabis>

## Learn

- Rust: <https://www.rust-lang.org/>
- Introduction Book (Free!): <https://doc.rust-lang.org/book/>
- Rust Standard Library: <https://doc.rust-lang.org/std/index.html>
- Rustlings    (Exercices): <https://github.com/rust-lang/rustlings/>
- lifetimekata (Exercices): <https://github.com/tfpk/lifetimekata>

## Videos

- Jon Gjengset: <https://www.youtube.com/@jonhoo>
- ThePrimeagen: <https://www.youtube.com/@ThePrimeagen>
