fn take_ownership_of_list(list: Vec<i32>) {
    println!("{:?}", list);
}

fn borrow_list(list: &Vec<i32>) {
    println!("{:?}", list);
}

fn main() {
    let mut a = vec![1, 2, 3];
    let mut b = a;

    a.push(4); // uh ho ...

    take_ownership_of_list(b);
    b.push(5); // nope!

    let mut c = vec![1, 2, 3];
    borrow_list(&c);

    c.push(4); // ok!
}

fn main() {
    let mut a = vec![1, 2, 3];
    // 'a' appartient à 'b'.
    let mut b = a;

    a.push(4); // nope ! l'ownership de 'a' a été transféré à 'b'.
}

fn main() {
    let mut a = vec![1, 2, 3];

    a.push(4); // ok!

    // nope ! l'ownership de 'a' a été transféré dans la fonction 'take_ownership_of_list'.
    take_ownership_of_list(a);
}

fn main() {
    let mut a = vec![1, 2, 3];

    // On utilise une référence '&' pour 'borrow' une variable.
    borrow_list(&a);

    a.push(4); // ok!
}
