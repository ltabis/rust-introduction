use std::error::Error;

fn main() {
    let x = Option::None;
}

enum Option<T> {
    Some(T),
    None,
}

enum Result<T, E> {
    Ok(T),
    Err(E),
}

fn get(v: &Vec<i32>, index: usize) -> Option<i32> {
    if index < v.len() {
        Some(v[index])
    } else {
        None
    }
}

fn read_file(path: &str) -> Result<String, MyError> {
    match std::fs::read_to_string(path) {
        Ok(file_content) => Ok(file_content),
        Err(error) => Err(MyError::FailedToRead(error)),
    }
}
