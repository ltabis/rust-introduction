fn main() {
    let pomme = Fruit::Pomme {
        kcal: 54.90,
        fibres: 2.5,
        glucides: 11.7,
    };

    print_fruit(&pomme);
}

enum Fruit {
    Banane,
    Concombre,
    Pomme {
        kcal: f64,
        fibres: f64,
        glucides: f64,
    },
}

fn print_fruit(fruit: &Fruit) {
    match fruit {
        Fruit::Banane => println!("Banana!"),
        Fruit::Concombre => println!("C'est un fruit ça ?"),
        Fruit::Pomme {
            kcal,
            fibres,
            glucides,
        } => println!(
            r#"
        Une pomme contient {fibres}g de fibres
        et {glucides}g de glucides.
        Une pomme fait {kcal}kcal."#
        ),
    }
}
