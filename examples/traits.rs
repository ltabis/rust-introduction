fn main() {
    let pomme = Pomme {};
    print_fruit(&pomme);

    let voiture = Voiture {};
    // nuh-hu!
    print_fruit(&voiture);
}

fn print_fruit<T: Fruit>(fruit: &T) {
    fruit.print()
}

trait Fruit {
    fn print(&self);
    fn kcal(&self) -> f64;
    fn glucides(&self) -> f64;
    fn pomme(&self) -> f64;
}

pub struct Voiture {}
pub struct Pomme {}

impl Fruit for Pomme {
    fn print(&self) {
        println!(
            r#"
        Une pomme contient 54.90g de fibres
        et 2.5g de glucides.
        Une pomme fait 11.7kcal."#
        );
    }

    fn kcal(&self) -> f64 {
        54.90
    }

    fn glucides(&self) -> f64 {
        2.5
    }

    fn pomme(&self) -> f64 {
        11.7
    }
}
