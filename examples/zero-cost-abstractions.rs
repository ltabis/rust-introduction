fn main() {
    no_abstractions();
    with_abstractions();
}

fn no_abstractions() {
    let mut numbers = vec![];

    for n in [1, 2, 3, 4, 5] {
        numbers.push(n * 2);
    }

    dbg!(numbers);
}

fn with_abstractions() {
    #[rustfmt::skip] // ignore that ;)
    let numbers = [1, 2, 3, 4, 5]
        .iter()
        .map(|n| n * 2)
        .collect::<Vec<_>>();

    dbg!(numbers);
}
