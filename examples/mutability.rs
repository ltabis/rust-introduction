#![allow(unused_assignments)]
#![allow(unused_variables)]
#![allow(dead_code)]

fn main() {
    let mut number = 1;

    number = 2;

    dbg!(number);
}

fn mutate_my_number(mut number: i32) {
    number += 1;
}

fn dont_mutate_my_number(number: i32) {
    // nope!
}
