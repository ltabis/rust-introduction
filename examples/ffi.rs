extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        println!("Valeur absolue de -3 d'apprêt la libc: {}", abs(-3));
    }
}

/// Accessible depuis un programme C !
#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Hello from Rust!");
}
