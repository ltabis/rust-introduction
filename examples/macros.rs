use rust_lisp::lisp;

fn main() {
    lisp! {
      (list
        (* 1 2)
        (/ 6 3 "foo"))
    };
}

#[route(GET, "/")]
fn index() {}

#[derive(Debug, Default, Clone)]
pub struct Configuration {
    // ...
}

fn sql() {
    let sql = sql!(SELECT * FROM posts WHERE id=1);
}
